
// We gotta use SoftwareServo because normal Servo disables the use of pins 9 and 10 as PWM
#include <SoftwareServo.h>

#include "pitches.h"

int LED1R = 9;
int LED1G = 10;
int LED1B = 11;

int LED2R = 3;
int LED2G = 5;
int LED2B = 6;

int Speaker = 4;

int IR = A0;
int FRS = A1;

// IR Votlages, closer to 3 is clsoer
float LVL1 = 1.65;
float LVL2 = 1.9;
float LVL3 = 2.6;
float LVL4 = 3;

float DEAD_LOW = 1.35;
float DEAD_HIGH = 1.55;

int ARM_PIN = A2;
int STAB_PIN = A3;
SoftwareServo arm;
SoftwareServo stab;

int ARM_L = 0;
int ARM_H = 35;

int STAB_L = 53;
int STAB_H = 90;

void setup() {
  pinMode(LED1R, OUTPUT);
  pinMode(LED1G, OUTPUT);
  pinMode(LED1B, OUTPUT);
  pinMode(LED2R, OUTPUT);
  pinMode(LED2G, OUTPUT);
  pinMode(LED2B, OUTPUT);

  pinMode(Speaker, OUTPUT);
  digitalWrite(Speaker, LOW);

  pinMode(IR, INPUT);
  pinMode(FRS, INPUT);

  arm.attach(ARM_PIN);
  stab.attach(STAB_PIN);

  arm.write(ARM_L);
  stab.write(STAB_L);

//  Serial.begin(9600);
}

void LED1(int R, int G, int B) {
  analogWrite(LED1R, 255-R);
  analogWrite(LED1G, 255-G);
  analogWrite(LED1B, 255-B);
}

void LED2(int R, int G, int B) {
  analogWrite(LED2R, 255-R);
  analogWrite(LED2G, 255-G);
  analogWrite(LED2B, 255-B);
}

#define STAB_MODE 0
#define WARN_FAR 1
#define WARN_CLOSE 2
#define IDLE_MODE 3

int checkIR() {
  float voltage = analogRead(IR) * (5.0 / 1023.0);
  Serial.println(voltage);
  if (voltage < LVL1 ) {
    return IDLE_MODE;
  } else if (voltage < LVL2) {
    return WARN_FAR;
  } else if (voltage < LVL3) {
    return WARN_CLOSE;
  } else {
    return STAB_MODE;
  }
}

void loop() {
  int state = checkIR();

  if (state == STAB_MODE) {
    LED1(255, 255, 0);
    LED2(255, 255, 0); // Yellow Eyes
    stabAction();
    fanfare();
  } else if (state == WARN_CLOSE) {
    flashEyes(10);
  } else if (state == WARN_FAR) {
    flashEyes(20);
  } else {
    LEDshow();
  }
}

void fanfare() {
  int As = 932;
  int C = 1047;
  int D = 1175;

  int As_t = 1073;
  int C_t = 955;
  int D_t = 851;

  pinMode(Speaker, OUTPUT);
  SoftTone(D_t, 156); // 10/64th's
  SoftTone(D_t, 156); // 10/64th's
  SoftTone(D_t, 156); // 10/64th's
  SoftTone(D_t, 500); // 1/2 second

  SoftTone(As_t, 500);
  SoftTone(C_t, 500);
  SoftTone(D_t, 188);
  delay(125);
  SoftTone(C_t, 156);
  SoftTone(D_t, 156);
}

void SoftTone(unsigned int period_us, unsigned int delay_ms) {
  unsigned long start = millis();
    while (start + delay_ms > millis()) {
    digitalWrite(Speaker, HIGH);
    delayMicroseconds(period_us/2);
    digitalWrite(Speaker, LOW);
    delayMicroseconds(period_us/2);
  }
  digitalWrite(Speaker, LOW);
  delay(15); //1/64th pause after note.
}

void servoDelay(int delay_t) {
  long start = millis();
  while (start + delay_t > millis()) {
    SoftwareServo::refresh();
    delay(1);
  }
}

void stabAction() {
 // Raise ARM
 arm.write(ARM_H); servoDelay(150);
 //Stab
 stab.write(STAB_H); servoDelay(1500);
 stab.write(STAB_L); servoDelay(1750);
 //Lower Arm
 for (int i = ARM_H; i != ARM_L; i--) {
   arm.write(i);
   servoDelay(8);
 }
}

void flashEyes(int delay_ms) {
  static int mode = 0;
  static int cnt = 1;

  cnt*=2;
  if (cnt > 255) {
    cnt = 1;
    mode++;
  }

  switch (mode) {
    case 0:
      cnt = 1;
      mode++;
      break;
    case 1:  // Fade In
      LED1(cnt, cnt/2, 0);
      LED2(cnt, cnt/2, 0);
      break;
    case 2: // Fade Out
      LED1(255/cnt, 128/cnt, 0);
      LED2(255/cnt, 128/cnt, 0);
      break;
    default:
      mode = 0;
  }
  delay (delay_ms);
}

void LEDshow() {
  static int mode = 0;
  static int cnt = 1;

  cnt*=2;
  if (cnt > 255) {
    cnt = 1;
    mode++;
  }

  switch (mode) {
  case 0:
    cnt = 1;
    mode++;
    break;
  case 1:  // Fade Blue in
    LED1(0, 0, cnt);
    LED2(0, 0, 255/cnt);
    break;
  case 2: // Fade Blue Out
    LED1(0, 0, 255/cnt);
    LED2(0, 0, cnt);
    break;
  case 3:  // Fade Green in
    LED1(0, cnt, 0);
    LED2(0, 255/cnt, 0);
    break;
  case 4: // Fade Green Out
    LED1(0, 255/cnt, 0);
    LED2(0, cnt, 0);
    break;
  case 5:  // Fade Red in
    LED1(cnt, 0, 0);
    LED2(255/cnt, 0, 0);
    break;
  case 6: // Fade Red Out
    LED1(255/cnt, 0 , 0);
    LED2(cnt, 0, 0);
    break;
  case 7:  // Fade Purple in
    LED1(cnt, 0, cnt);
    LED2(255/cnt, 0, 255/cnt);
    break;
  case 8: // Fade Purple Out
    LED1(255/cnt, 0 , 255/cnt);
    LED2(cnt, 0, cnt);
    break;
  case 9:  // Fade White in
    LED1(cnt, cnt, cnt);
    LED2(255/cnt, 255/cnt, 255/cnt);
    break;
  case 10: // Fade White Out
    LED1(255/cnt, 255/cnt , 255/cnt);
    LED2(cnt, cnt, cnt);
    break;
  default:
    mode = 0;
  }
  delay (40);
}

